#pragma once

class Timer {
public:
    Timer();
    ~Timer();

    void start();
    void stop();

    double getElapsedMicroseconds();

private:
    Timer(Timer const& timer) : internal(0) { }
    void operator= (Timer const& timer) { }

    struct Internal;
    Internal* internal;
};
