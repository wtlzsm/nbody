#include "timer.h"

#ifdef _WIN32
    #include <windows.h>
#else
    #include <sys/time.h>
#endif

#include <cstdlib>
#include <cassert>

struct Timer::Internal {
    double startTimeInMicroSec;
    double endTimeInMicroSec;
    int stopped;

    #ifdef _WIN32
        LARGE_INTEGER frequency;
        LARGE_INTEGER startCount;
        LARGE_INTEGER endCount;
    #else
        timeval startCount;
        timeval endCount;
    #endif
};

Timer::Timer() {
    internal = new Internal();

    #ifdef _WIN32
        QueryPerformanceFrequency(&internal->frequency);
        internal->startCount.QuadPart = 0;
        internal->endCount.QuadPart = 0;
    #else
        internal->startCount.tv_sec = internal->startCount.tv_usec = 0;
        internal->endCount.tv_sec = internal->endCount.tv_usec = 0;
    #endif

    internal->stopped = 0;
    internal->startTimeInMicroSec = 0;
    internal->endTimeInMicroSec = 0;
}

Timer::~Timer() {
    delete internal;
}

void Timer::start() {
    assert(internal != NULL);
    internal->stopped = 0; // reset stop flag
    #ifdef WIN32
        QueryPerformanceCounter(&internal->startCount);
    #else
        gettimeofday(&internal->startCount, NULL);
    #endif
}

void Timer::stop() {
    assert(internal != NULL);
    internal->stopped = 1; // set timer stopped flag

    #ifdef WIN32
        QueryPerformanceCounter(&internal->endCount);
    #else
        gettimeofday(&internal->endCount, NULL);
    #endif
}

double Timer::getElapsedMicroseconds() {
    #ifdef WIN32
        if (!internal->stopped) {
            QueryPerformanceCounter(&internal->endCount);
        }

        internal->startTimeInMicroSec = internal->startCount.QuadPart * (1000000.0 / internal->frequency.QuadPart);
        internal->endTimeInMicroSec = internal->endCount.QuadPart * (1000000.0 / internal->frequency.QuadPart);
    #else
        if (!internal->stopped) {
            gettimeofday(&internal->endCount, NULL);
        }

        internal->startTimeInMicroSec = (internal->startCount.tv_sec * 1000000.0) + internal->startCount.tv_usec;
        internal->endTimeInMicroSec = (internal->endCount.tv_sec * 1000000.0) + internal->endCount.tv_usec;
    #endif

    return internal->endTimeInMicroSec - internal->startTimeInMicroSec;
}
