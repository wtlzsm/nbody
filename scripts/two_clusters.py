#!/usr/bin/env python

from __future__ import division
import numpy as np
import numpy.random as nprand
from collections import namedtuple

def random_sphere_point(radius, coords="cartesian"):
    """
    See: http://www.ece.virginia.edu/mv/edu/prob/stat/random-number-generation.pdf
    """
    r = radius * nprand.uniform() ** (1/3)
    phi = nprand.uniform() * 2 * np.pi
    theta = np.arccos(1 - 2 * nprand.uniform())

    if coords == "polar":
        return np.array([r, phi, theta])
    elif coords == "cartesian":
        z = r * np.cos(theta)
        x = r * np.sin(theta) * np.cos(phi)
        y = r * np.sin(theta) * np.sin(phi)
        return np.array([x, y, z])

Cluster = namedtuple('Cluster', ['position', 'velocity', 'radius', 'particles', 'color'])
Particle = namedtuple('Particle', ['mass', 'position', 'velocity', 'color'])

clusters = [
    Cluster(position=np.array([30000, 40000, 0]),
            velocity=np.array([0, 0, 0]), 
            radius=10000, 
            particles=250,
            color=[255, 0, 0]),
    Cluster(position=np.array([90000, 40000, 0]), 
            velocity=np.array([0, 0, 0]),
            radius=10000, 
            particles=250,
            color=[0, 255, 0])
]

def format_particle_info(mass, position, velocity, color):
    fmt = "{ mass = %f; position = [%f, %f, %f]; velocity = [%f, %f, %f]; color = [%d, %d, %d]; }"
    return fmt % (mass, 
        position[0], position[1], position[2], 
        velocity[0], velocity[1], velocity[2],
        color[0], color[1], color[2])

def print_particles(particles):
    print "particles : ("
    for i in range(len(particles)):
        p = particles[i]
        line = format_particle_info(p.mass, p.position, p.velocity, p.color)
        if i != len(particles) - 1:
            line += ","
        print line
    print");"

particles = []
for cluster in clusters:
    for i in xrange(cluster.particles):
        mass = 1
        position = cluster.position + random_sphere_point(cluster.radius)
        velocity = cluster.velocity + random_sphere_point(0.001)
        color = cluster.color

        particles.append(Particle(mass, position, velocity, color))
print_particles(particles)