#include "core/core.h"

#include <libconfig.h++>
#include <GL/glut.h>

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>

#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cmath>
#include <cstdint>

//1 pixel = 1000 km = 0.6667e-5 a.e.
const int SCREEN_WIDTH = 1024;
const int SCREEN_HEIGHT = 768;

bool LoadConfig(char const* path, ModelData* modelData) {
    libconfig::Config config;
    config.setIncludeDir(".");
    config.readFile(path);

    libconfig::Setting& particles = config.lookup("particles");
    if (!particles.isList()) {
        fprintf(stderr, "Config doesn't contain particle data.\n");
        return false;
    }

    size_t particleCount = particles.getLength();
    modelData->particles.resize(particleCount);
    modelData->tmp1.resize(particleCount);
    modelData->tmp2.resize(particleCount);

    for (size_t i = 0; i < particleCount; i++) {
        Particle& particle = modelData->particles[i];

        particle.m = particles[i]["mass"];

        particle.p[0] = particles[i]["position"][0];
        particle.p[1] = particles[i]["position"][1];
        particle.p[2] = particles[i]["position"][2];

        particle.v[0] = particles[i]["velocity"][0];
        particle.v[1] = particles[i]["velocity"][1];
        particle.v[2] = particles[i]["velocity"][2];

        particle.red = static_cast<unsigned int>(particles[i]["color"][0]);
        particle.green = static_cast<unsigned int>(particles[i]["color"][1]);
        particle.blue = static_cast<unsigned int>(particles[i]["color"][2]);
    }

    return true;
}

int main(int argc, char** argv) {
    if (argc < 2) {
        fprintf(stderr, "Not enough arguments.\n");
        return EXIT_FAILURE;
    }

    char const* configPath = argv[1];
    if (!LoadConfig(configPath, &data)) {
        fprintf(stderr, "Could not parse configuration file.\n");
        return EXIT_FAILURE;
    }

    InitializeParticles();

    uint64_t step = 0;
    while (true) {
        TimeStep();

        if (step % 1000 == 0) {
            long double time = data.time;

            double energy = CalculateTotalEnergy(&data.particles[0], data.particles.size());
            Eigen::Vector3d momentum = CalculateTotalMomentum(&data.particles[0], data.particles.size());
            Eigen::Vector3d angularMomentum = CalculateTotalAngularMomentum(&data.particles[0], data.particles.size());

            printf("%Lf\t", time);
            printf("%f\t", energy);
            printf("%f\t%f\t%f\t", momentum[0], momentum[1], momentum[2]);
            printf("%f\t%f\t%f\n", angularMomentum[0], angularMomentum[1], angularMomentum[2]);
        }

        step++;
    }
}
