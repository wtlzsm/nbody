#include "core/core.h"

#include <libconfig.h++>
#include <GL/glut.h>

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>

#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cmath>
#include <cstdint>

//1 pixel = 1000 km = 0.6667e-5 a.e.
const int SCREEN_WIDTH = 1024;
const int SCREEN_HEIGHT = 768;

bool LoadConfig(char const* path, ModelData* modelData) {
    libconfig::Config config;
    config.setIncludeDir(".");
    config.readFile(path);

    libconfig::Setting& particles = config.lookup("particles");
    if (!particles.isList()) {
        fprintf(stderr, "Config doesn't contain particle data.\n");
        return false;
    }

    size_t particleCount = particles.getLength();
    modelData->particles.resize(particleCount);
    modelData->tmp1.resize(particleCount);
    modelData->tmp2.resize(particleCount);

    for (size_t i = 0; i < particleCount; i++) {
        Particle& particle = modelData->particles[i];

        particle.m = particles[i]["mass"];

        particle.p[0] = particles[i]["position"][0];
        particle.p[1] = particles[i]["position"][1];
        particle.p[2] = particles[i]["position"][2];

        particle.v[0] = particles[i]["velocity"][0];
        particle.v[1] = particles[i]["velocity"][1];
        particle.v[2] = particles[i]["velocity"][2];

        particle.red = static_cast<unsigned int>(particles[i]["color"][0]);
        particle.green = static_cast<unsigned int>(particles[i]["color"][1]);
        particle.blue = static_cast<unsigned int>(particles[i]["color"][2]);
    }

    return true;
}

void OnResize(GLint x, GLint y) {
    glViewport(0, 0, x, y);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, x, 0, y);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void OnKeyPress(unsigned char key, int, int) {
    if (key == 'a') {
        exit(0);
    }
}

void OnTimer(int a) {
    TimeStep();
    glutPostRedisplay();
    glutTimerFunc(0, OnTimer, 1);
}

void RenderParticle(Particle const& particle) {
    glBegin(GL_POINTS);
    glColor3ub(particle.red, particle.green, particle.blue);
    glVertex2d(particle.p[0] / 100, particle.p[1] / 100);
    glEnd();
}

void RenderScene() {
    std::vector<Particle>& particles = data.particles;

    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    for(size_t i = 0; i < particles.size(); i++) {
        RenderParticle(particles[i]);
    }

    glFinish();
}

int main(int argc, char** argv) {
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_RGB);
    glutInitWindowSize(SCREEN_WIDTH, SCREEN_HEIGHT);
    glutCreateWindow("Particle motion");

    glutDisplayFunc(RenderScene);
    glutReshapeFunc(OnResize);
    glutKeyboardFunc(OnKeyPress);
    glutTimerFunc(50, OnTimer, 1);

    if (argc < 2) {
        fprintf(stderr, "Not enough arguments.\n");
        return EXIT_FAILURE;
    }

    char const* configPath = argv[1];
    if (!LoadConfig(configPath, &data)) {
        fprintf(stderr, "Could not parse configuration file.\n");
        return EXIT_FAILURE;
    }

    InitializeParticles();

    glutMainLoop();
    getchar();
}
