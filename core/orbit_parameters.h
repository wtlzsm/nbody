#pragma once

#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Geometry>

void cartel (
    double m1,
    double m2,
    Eigen::Vector3d const& p1,
    Eigen::Vector3d const& p2,
    Eigen::Vector3d const& v1,
    Eigen::Vector3d const& v2,
    double* a1,
    double* a2,
    double* e,
    Eigen::Vector3d* n,
    double* totn,
    Eigen::Vector3d* pc,
    Eigen::Vector3d* vc,
    Eigen::Vector3d* pulse);

void carthyp(
    double m1,
    double m2,
    Eigen::Vector3d const& p1,
    Eigen::Vector3d const& p2,
    Eigen::Vector3d const& v1,
    Eigen::Vector3d const& v2,
    double* a1,
    double* a2,
    double* e,
    Eigen::Vector3d* n,
    double* totn,
    Eigen::Vector3d* pc,
    Eigen::Vector3d* vc,
    Eigen::Vector3d* pulse);

void elcart(
    double a1,
    double a2,
    double e,
    Eigen::Vector3d const& n,
    double totn,
    Eigen::Vector3d const& pc,
    Eigen::Vector3d const& vc,
    Eigen::Vector3d const& pulse,
    double m1,
    double m2,
    Eigen::Vector3d* p1,
    Eigen::Vector3d* p2,
    Eigen::Vector3d* v1,
    Eigen::Vector3d* v2);

void hypcart(
    double a1,
    double a2,
    double e,
    Eigen::Vector3d const& n,
    double totn,
    Eigen::Vector3d const& pc,
    Eigen::Vector3d const& vc,
    Eigen::Vector3d const& p,
    double m1,
    double m2,
    Eigen::Vector3d* p1,
    Eigen::Vector3d* p2,
    Eigen::Vector3d* v1,
    Eigen::Vector3d* v2);
