#include "partial_sort.h"

#include "gtest/gtest.h"

size_t Count(double* array, size_t len, double value) {
    assert(array != NULL);

    size_t result = 0;

    for (size_t i = 0; i < len; i++) {
        if (array[i] == value) {
            result += 1;
        }
    }

    return result;
}

TEST(PartialSortTest, WrongArguments) {
  ASSERT_DEATH(PartialSort(NULL, 0, 10), "Assertion");

  double val;
  ASSERT_DEATH(PartialSort(&val, 2, 1), "Assertion");
}

TEST(PartialSortTest, 3Elements) {
    {
        double a[] = { 1.0, 0.0, 2.0 };
        PartialSort(a, 1, 3);
        EXPECT_EQ(0.0, a[0]);
    }

    {
        double a[] = { 0.0, 1.0, 2.0 };
        PartialSort(a, 1, 3);
        EXPECT_EQ(0.0, a[0]);
    }

    {
        double a[] = { 2.0, 1.0, 0.0 };
        PartialSort(a, 1, 3);
        EXPECT_EQ(0.0, a[0]);
    }
}

TEST(PartialSortTest, 10Elements) {
    {
        double a[] = {0.628224, 0.523477, 0.509851, 0.110291, 0.323137,
                      0.864075, 0.268953, 0.721538, 0.437013, 0.317826};
        PartialSort(a, 1, 10);

        EXPECT_EQ(1, Count(a, 1, 0.110291));
    }

    {
        double a[] = {0.628224, 0.523477, 0.509851, 0.110291, 0.323137,
                      0.864075, 0.268953, 0.721538, 0.437013, 0.317826};
        PartialSort(a, 3, 10);

        EXPECT_EQ(1, Count(a, 3, 0.110291));
        EXPECT_EQ(1, Count(a, 3, 0.268953));
        EXPECT_EQ(1, Count(a, 3, 0.317826));
    }

    {
        double a[] = {0.628224, 0.523477, 0.509851, 0.110291, 0.323137,
                      0.864075, 0.268953, 0.721538, 0.437013, 0.317826};
        PartialSort(a, 4, 10);

        EXPECT_EQ(1, Count(a, 4, 0.110291));
        EXPECT_EQ(1, Count(a, 4, 0.268953));
        EXPECT_EQ(1, Count(a, 4, 0.317826));
        EXPECT_EQ(1, Count(a, 4, 0.323137));
    }
}

TEST(PartialSortTest, 10ElementsSameValues) {
    {
        double a[] = {0.317826, 0.523477, 0.509851, 0.110291, 0.323137,
                      0.110291, 0.268953, 0.721538, 0.437013, 0.317826};
        PartialSort(a, 1, 10);

        EXPECT_EQ(1, Count(a, 1, 0.110291));
    }

    {
        double a[] = {0.317826, 0.523477, 0.509851, 0.110291, 0.323137,
                      0.110291, 0.268953, 0.721538, 0.437013, 0.317826};
        PartialSort(a, 2, 10);

        EXPECT_EQ(2, Count(a, 2, 0.110291));
    }

    {
        double a[] = {0.317826, 0.523477, 0.509851, 0.110291, 0.323137,
                      0.110291, 0.268953, 0.721538, 0.437013, 0.317826};
        PartialSort(a, 3, 10);

        EXPECT_EQ(2, Count(a, 3, 0.110291));
        EXPECT_EQ(1, Count(a, 3, 0.268953));
    }

    {
        double a[] = {0.317826, 0.523477, 0.509851, 0.110291, 0.323137,
                      0.110291, 0.268953, 0.721538, 0.437013, 0.317826};
        PartialSort(a, 4, 10);

        EXPECT_EQ(2, Count(a, 4, 0.110291));
        EXPECT_EQ(1, Count(a, 4, 0.268953));
        EXPECT_EQ(1, Count(a, 4, 0.317826));
    }
}

TEST(GenericPartialSortTest, 10Elements) {
    {
        double a[] = {0.628224, 0.523477, 0.509851, 0.110291, 0.323137,
                      0.864075, 0.268953, 0.721538, 0.437013, 0.317826};
        PartialSort(a, a + 1, a + 10);

        EXPECT_EQ(1, Count(a, 1, 0.110291));
    }

    {
        double a[] = {0.628224, 0.523477, 0.509851, 0.110291, 0.323137,
                      0.864075, 0.268953, 0.721538, 0.437013, 0.317826};
        PartialSort(a, a + 3, a + 10);

        EXPECT_EQ(1, Count(a, 3, 0.110291));
        EXPECT_EQ(1, Count(a, 3, 0.268953));
        EXPECT_EQ(1, Count(a, 3, 0.317826));
    }

    {
        double a[] = {0.628224, 0.523477, 0.509851, 0.110291, 0.323137,
                      0.864075, 0.268953, 0.721538, 0.437013, 0.317826};
        PartialSort(a, a + 4, a + 10);

        EXPECT_EQ(1, Count(a, 4, 0.110291));
        EXPECT_EQ(1, Count(a, 4, 0.268953));
        EXPECT_EQ(1, Count(a, 4, 0.317826));
        EXPECT_EQ(1, Count(a, 4, 0.323137));
    }
}
