#include "kepler_equations.h"

#include <cassert>
#include <cmath>
#include <cstdio>

// Solves Kepler's equation for elliptic orbit.
// @see https://en.wikipedia.org/wiki/Kepler%27s_Equation
double ksiel(double meanAnomaly, double eccentricity) {
    assert(0 <= eccentricity && eccentricity < 1);

    double M = meanAnomaly;

    // Estimated value of eccentric anomaly.
    double e = M;

    while(true) {
        double fe0 = e - eccentricity * std::sin(e) - M;
        double fe1 = 1 - eccentricity * std::cos(e);
        double fe2 = -fe0 + e;

        e = e - 5 * fe0 / (fe1 + 2 * std::sqrt(std::abs(4 * fe1 * fe1 - 5 * fe0 * fe2)));

        double m = e - eccentricity * std::sin(e);

        if (std::abs(M - m) < 1e-12) {
            break;
        }
    }

    return e;
}

// Solves Hyperbolic Kepler's equation for elliptic orbit.
// @see https://en.wikipedia.org/wiki/Kepler%27s_Equation
double ksihyp(double meanAnomaly, double eccentricity) {
    assert(eccentricity > 1);

    double M = meanAnomaly;

    // Estimated value of eccentric anomaly.
    double e;
    if (std::abs(M) < 1e-12) {
        e = 0;
    } else {
        e = M / std::abs(M) * std::log(1 + std::abs(M));
    }

    while (true) {
        double sinh = std::sinh(e);
        double cosh = std::cosh(e);

        double fe0 = eccentricity * sinh - e - M;
        double fe1 = eccentricity * cosh;
        double fe2 = eccentricity * sinh;

        e = e - 5 * fe0 / (fe1 + 2 * std::sqrt(std::abs(4 * fe1 * fe1 - 5 * fe0 * fe2)));

        double m = eccentricity * sinh - e;

        if (std::abs(M - m) < 1e-12) {
            break;
        }
    }

    return e;
}
