#include "core.h"

#include <limits>

#include "partial_sort.h"
#include "kepler_equations.h"
#include "orbit_parameters.h"

ModelData data;

void TaylorPosition(Particle* particle, double dt){
    particle->p += particle->v * dt + particle->w * dt * dt / 2 + particle->w1 * dt * dt * dt / 6
        + particle->w2 * dt * dt * dt * dt / 24;
}

void AdvanceVelocity(Particle* particle, double dt) {
    particle->v += particle->w * dt + particle->w1 * dt * dt / 2 + particle->w2 * dt * dt * dt / 6;
}

// FIXME[alexknvl]: A better name?
void AdvancePosition(Particle* particle, double dt) {
    particle->p += particle->w * dt * dt / 2 + particle->w1 * dt * dt * dt / 6
        + particle->w2 * dt * dt * dt * dt / 24;
}

void FindNearest99Neighbours(size_t j) {
    std::vector<double>& rff = data.tmp1;
    std::vector<double>& rf = data.tmp2;
    std::vector<Particle>& particles = data.particles;

    assert(rff.size() == particles.size());
    assert(rf.size() == particles.size());
    size_t particleCount = particles.size();

    for (size_t i = 0; i < particleCount; i++) {
        rff[i] = (particles[i].p - particles[j].p).squaredNorm();
        rf[i] = rff[i];
    }

    PartialSort(rff.begin(), rff.begin() + 99, rff.end());

    for (size_t i = 0, p = 0; i < particleCount; i++) {
        if (rf[i] <= rff[99] && rf[i] != 0) {
            particles[j].numf[p] = i;
            p++;
        }
    }
}

void FindNearest3Neighbours(size_t p) {
    std::vector<Particle>& particles = data.particles;
    std::vector<double>& r5 = data.tmp1;

    double minDistance = 0;
    
    for (size_t j1 = 0; j1 < 99; j1++) {
        size_t i = particles[p].numf[j1];

        r5[i] = (particles[i].p - particles[p].p).norm();
    }

    for (size_t i = 0; i < 3; i++) {
        double rt = 1e10;

        for (size_t j1 = 0; j1 < 99; j1++) {
            size_t j = particles[p].numf[j1];

            if (j != p) {
                if ((r5[j] < rt) && (r5[j] > minDistance)) {
                    particles[p].num5[i] = j;
                    rt = r5[j];
                }
            }
        }
        
        size_t k = particles[p].num5[i];
        minDistance = r5[k];
    }
}

void correction(
    Eigen::Vector3d const& ww,
    Eigen::Vector3d const& ww1,
    Eigen::Vector3d const& ww2,
    double dt,
    Particle* particle)
{
    particle->p += dt * (particle->v + dt * (ww / 2 + dt * (ww1 / 6 + ww2 * dt / 24)));
    particle->v += dt * (ww + dt * (ww1 / 2 + ww2 * dt / 6));
    particle->currt+=dt;
}

void TimeSync(void)
{
    std::vector<Particle>& particles = data.particles;
    size_t particleCount = particles.size();
    
    double AverageTime=0;
    for(size_t i=0; i<particleCount; i++)
	{
	AverageTime+=particles[i].currt;
	}
    AverageTime=AverageTime/particleCount;
    for(size_t i=0; i<particleCount; i++)
	{
	TaylorPosition(&particles[i], (AverageTime-particles[i].currt));
	AdvanceVelocity(&particles[i], (AverageTime-particles[i].currt));
	}
    for(size_t i=0; i<particleCount; i++)
        {
        particles[i].currt=AverageTime;
        }
}


double AarsethTimeStep(
    Eigen::Vector3d const& velocity,
    Eigen::Vector3d const& acceleration,
    Eigen::Vector3d const& jerk,
    Eigen::Vector3d const& snap)
{
    const double NU = 0.02;

    double m = velocity.norm() * jerk.norm() + acceleration.squaredNorm();
    double k = acceleration.norm() * snap.norm() + jerk.squaredNorm();

    double result = sqrt(NU * m / k);
    
    if (isinf(result) || result > 1e8) {
        result = 1000.0;
    }

    assert(!::isnan(result));

    return result;
}

// Calculates acceleration, jerk and snap of the first particle caused by the second particle.
void CalculateAcceleration(
    Particle const& first,
    Particle const& second,
    Eigen::Vector3d* acceleration)
{
    Eigen::Vector3d r = second.p - first.p;

    double d = r.norm();
    double k = second.m / (d * d * d);

    *acceleration = k * r;
}

// Calculates jerk and snap of the first particle caused by the second particle.
void CalculateJerkAndSnap(
    Particle const& first,
    Particle const& second,
    Eigen::Vector3d* jerk,
    Eigen::Vector3d* snap)
{
    Eigen::Vector3d r = second.p - first.p;
    Eigen::Vector3d v = second.v - first.v;
    Eigen::Vector3d a = second.w - first.w;

    double d = r.norm();
    double k = second.m / (d * d * d);

    double rv = r.dot(v);
    double vv = v.dot(v);
    double ra = r.dot(a);

    *jerk = k * (v - 3 / (d * d) * r * rv);
    *snap = k * (a - 3 / (d * d) * (2 * rv * v + ra * r + vv * r - 5 / (d * d) * (rv * rv * r)));
}

void faraccel(size_t i) {
    assert(i < data.particles.size());

    std::vector<Particle>& particles = data.particles;

    particles[i].w5 = particles[i].wf;
    particles[i].w51 = particles[i].wf1;
    particles[i].w52 = particles[i].wf2;

    for (size_t j = 0; j < 99; j++) {
        if ((particles[i].numf[j] != i) &&
            (particles[i].numf[j] != particles[i].num5[0]) &&
            (particles[i].numf[j] != particles[i].num5[1]) &&
            (particles[i].numf[j] != particles[i].num5[2]))
        {
            size_t k = particles[i].numf[j];

            Eigen::Vector3d acceleration, jerk, snap;
            CalculateAcceleration(particles[i], particles[k], &acceleration);
            CalculateJerkAndSnap(particles[i], particles[k], &jerk, &snap);

            particles[i].w5 += acceleration;
            particles[i].w51 += jerk;
            particles[i].w52 += snap;
        }
    }

}

void faraccelf(size_t i) {
    assert(i < data.particles.size());

    std::vector<Particle>& particles = data.particles;
    std::vector<double>& tmp = data.tmp1;

    assert(tmp.size() == particles.size());
    size_t particleCount = particles.size();

    particles[i].wf = Eigen::Vector3d(0, 0, 0);
    particles[i].wf1 = Eigen::Vector3d(0, 0, 0);
    particles[i].wf2 = Eigen::Vector3d(0, 0, 0);

    for (size_t j = 0; j < particleCount; j++) {
        tmp[j] = 1;
    }

    for (size_t j = 0; j < 99; j++) {
        size_t k = particles[i].numf[j];
        tmp[k] = -1;
    }

    for (size_t j = 0; j < particleCount; j++) {
        if (i == j) {
            continue;
        }

        if (tmp[j] < 0) {
            continue;
        }

        Eigen::Vector3d acceleration, jerk, snap;
        CalculateAcceleration(particles[i], particles[j], &acceleration);
        CalculateJerkAndSnap(particles[i], particles[j], &jerk, &snap);

        particles[i].wf += acceleration;
        particles[i].wf1 += jerk;
        particles[i].wf2 += snap;
    }

}

void accel5(
    size_t numi,
    ssize_t& nmax,
    Eigen::Vector3d* wn,
    Eigen::Vector3d* wn1,
    Eigen::Vector3d* wn2)
{
    std::vector<Particle>& particles = data.particles;

    Particle& particle = particles[numi];

    particle.w = particle.w5;
    particle.w1 = particle.w51;
    particle.w2 = particle.w52;

    Eigen::Vector3d wnn(0, 0, 0);
    Eigen::Vector3d wnn1(0, 0, 0);
    Eigen::Vector3d wnn2(0, 0, 0);

    double wmax = 0;

    for (size_t j = 0; j < 3; j++) {
        size_t index = particle.num5[j];
        Particle& other = particles[index];

        Eigen::Vector3d acceleration, jerk, snap;
        CalculateAcceleration(particle, other, &acceleration);
        CalculateJerkAndSnap(particle, other, &jerk, &snap);

        particle.w += acceleration;
        particle.w1 += jerk;
        particle.w2 += snap;

        if (wmax < acceleration.norm()) {
            wmax = acceleration.norm();
            nmax = index;
            wnn = acceleration;
            wnn1 = jerk;
            wnn2 = snap;
        }
    }

    double dtt = -particles[nmax].gdto;
    assert(!isinf(dtt) && !isnan(dtt));

    particle.gdt = AarsethTimeStep(particles[nmax].v, particle.w, particle.w1, particle.w2);
    double dt = particle.gdt;
    assert(!isinf(dt) && !isnan(dt));

    Eigen::Vector3d wo = particle.w - wnn;
    Eigen::Vector3d wo1 = particle.w1 - wnn1;
    Eigen::Vector3d wo2 = particle.w2 - wnn2;

    double gt = AarsethTimeStep(particle.v, wo, wo1, wo2);

    if (gt / particle.gdt > 10) {
        *wn = particles[nmax].w5;
        *wn1 = particles[nmax].w51;
        *wn2 = particles[nmax].w52;
        
        for (size_t j = 0; j < 3; j++) {
            size_t j5 = particles[nmax].num5[j];
            
            if (j5 != numi) {
              Eigen::Vector3d acceleration, jerk, snap;
                CalculateAcceleration(particles[nmax], particles[j5], &acceleration);
                CalculateJerkAndSnap(particles[nmax], particles[j5], &jerk, &snap);

                *wn += acceleration;
                *wn1 += jerk;
                *wn2 += snap;
            }
        }

        double gtn = AarsethTimeStep(particles[nmax].v, *wn, *wn1, *wn2);
        
        if ((gtn / particles[nmax].gdt) <= 10) {
            particle.p += particle.v * dt;
            particle.currt+=dt;
            nmax = -1;
            return;
        }

        if (gt > gtn) {
            gt = gtn;
        }

        particle.gdt = gt;
        particles[nmax].gdt = gt;

        Eigen::Vector3d acceleration, jerk, snap;
        // FIXME[alexknvl]: Check if the order of parameters is correct.
        //   Should we calculate the acceleration of particles[nmax] caused by
        //   particles[numi], or the acceleration of particles[numi]?
        CalculateAcceleration(particles[nmax], particle, &acceleration);
        CalculateJerkAndSnap(particles[nmax], particle, &jerk, &snap);

        Eigen::Vector3d wc = (*wn) + acceleration;
        Eigen::Vector3d wc1 = (*wn1) + jerk;
        Eigen::Vector3d wc2 = (*wn2) + snap;

        if (abs(particle.gdto - particles[nmax].gdto) > 1e-12) {
            correction(wc, wc1, wc2, dtt, &particles[nmax]);
        }

        double r = (particle.p - particles[nmax].p).norm();

        particle.w = wo;
        particle.w1 = wo1;
        particle.w2 = wo2;

        double ep = -particles[nmax].m * particle.m / r;
        double ek = particle.m * particles[nmax].m / (particle.m + particles[nmax].m) * ((particle.v - particles[nmax].v).squaredNorm()) / 2;
        double en = ek + ep;

        double ao1;
        double ao2;
        double totn;
        double exc;

        Eigen::Vector3d n;
        Eigen::Vector3d pc;
        Eigen::Vector3d vc;
        Eigen::Vector3d pulse;

        if (en < 0) {
            cartel(particle.m, particles[nmax].m,
                   particle.p, particles[nmax].p,
                   particle.v, particles[nmax].v,
                   &ao1, &ao2, &exc, &n, &totn, &pc, &vc, &pulse);
            totn += particle.gdt;
            pc += vc * particle.gdt;
            particle.currt+=particle.gdt;
            particles[nmax].currt+=particles[nmax].gdt;
            elcart(ao1, ao2, exc, n, totn, pc, vc, pulse,
                   particle.m, particles[nmax].m,
                   &particle.p, &particles[nmax].p,
                   &particle.v, &particles[nmax].v);
        }

        if (en > 0) {
            carthyp(particle.m, particles[nmax].m,
                    particle.p, particles[nmax].p,
                    particle.v, particles[nmax].v,
                    &ao1, &ao2, &exc, &n, &totn, &pc, &vc, &pulse);
            totn += particle.gdt;
            pc += vc * particle.gdt;
            particle.currt+=particle.gdt;
            particles[nmax].currt+=particles[nmax].gdt;
            hypcart(ao1, ao2, exc, n, totn, pc, vc, pulse,
                    particle.m, particles[nmax].m,
                    &particle.p, &particles[nmax].p,
                    &particle.v, &particles[nmax].v);
        }
    } else {
        particle.p += particle.v * dt;
        particle.currt+=dt;
        nmax = -1;
    }
}

size_t GetParticleWithSmallestGDTO(std::vector<Particle> const& particles) {
    assert(particles.size() > 0);

    double minTime = std::numeric_limits<double>::max();
    size_t minIndex = 0;

    for (size_t i = 0; i < particles.size(); i++) {
        if (particles[i].gdto < minTime) {
            minTime = particles[i].gdto;
            minIndex = i;
        }
    }
    
    return minIndex;
}

Eigen::Vector3d CalculateCentreOfMassPosition(Particle const* particles, size_t count) {
  Eigen::Vector3d centre(0, 0, 0);
    double massSum = 0;

    TimeSync();
    for (size_t i = 0; i < count; i++) {
        centre += particles[i].m * particles[i].p;
        massSum += particles[i].m;
    }

    return centre / massSum;
}

double CalculateTotalEnergy(Particle const* particles, size_t count) {
    double energy = 0;

    TimeSync();
    for (size_t i = 0; i < count; i++) {
        energy += particles[i].m * particles[i].v.squaredNorm() / 2;

        for (size_t j = i + 1; j < count; j++) {
            Eigen::Vector3d r = particles[i].p - particles[j].p;
            energy -= particles[i].m * particles[j].m / r.norm();
        }
    }

    return energy;
}

Eigen::Vector3d CalculateTotalMomentum(Particle const* particles, size_t count) {
  Eigen::Vector3d momentum(0, 0, 0);

    TimeSync();
    for (size_t i = 0; i < count; i++) {
        momentum += particles[i].m * particles[i].v;
    }

    return momentum;
}

Eigen::Vector3d CalculateTotalAngularMomentum(Particle const* particles, size_t count) {
    Eigen::Vector3d angularMomentum(0, 0, 0);

    TimeSync();
    for (size_t i = 0; i < count; i++) {
        angularMomentum += particles[i].m * particles[i].p.cross(particles[i].v);
    }

    return angularMomentum;
}

void TimeStep() {
    std::vector<Particle>& particles = data.particles;
    size_t particleCount = particles.size();

    ssize_t numi = GetParticleWithSmallestGDTO(particles);
    double minTime = particles[numi].gdto;

    ssize_t nmax = -1;
    Eigen::Vector3d wn;
    Eigen::Vector3d wn1;
    Eigen::Vector3d wn2;

    if (particles[numi].gdtof > 0) {
        if (particles[numi].gdto5 > 0) {
            accel5(numi, nmax, &wn, &wn1, &wn2);
        } else {
            FindNearest3Neighbours(numi);
            faraccel(numi);
            particles[numi].gdto5 = AarsethTimeStep(particles[numi].v, particles[numi].w5, particles[numi].w51, particles[numi].w52);
            accel5(numi, nmax, &wn, &wn1, &wn2);
        }
    } else {
        FindNearest99Neighbours(numi);
        faraccelf(numi);
        particles[numi].gdtof = AarsethTimeStep(particles[numi].v, particles[numi].wf, particles[numi].wf1, particles[numi].wf2);
        faraccel(numi);

        if (particles[numi].gdto5 > 0) {
            accel5(numi, nmax, &wn, &wn1, &wn2);
        } else {
            FindNearest3Neighbours(numi);
            faraccel(numi);
            particles[numi].gdto5 = AarsethTimeStep(particles[numi].v, particles[numi].w5, particles[numi].w51, particles[numi].w52);
            accel5(numi, nmax, &wn, &wn1, &wn2);
        }
    }
    
    // FIXME[alexknvl]: A different magic constant.
    double dt = particles[numi].gdt;

    for (ssize_t i = 0; i < (ssize_t) particleCount; i++) {
        particles[i].gdto5 = particles[i].gdto5 - minTime;
        particles[i].gdtof = particles[i].gdtof - minTime;

        if ((i == numi) || (i == nmax)) {
            particles[i].gdto = particles[i].gdt;
        } else {
            particles[i].gdto = particles[i].gdto - minTime;
        }
    }
    
    if (nmax >= 0) {
        if (particles[nmax].gdtof < 0) {
            FindNearest99Neighbours(nmax);
            faraccelf(nmax);
            particles[nmax].gdtof = AarsethTimeStep(particles[nmax].v, particles[nmax].wf, particles[nmax].wf1, particles[nmax].wf2);
        }

        if (particles[nmax].gdto5 < 0) {
            FindNearest3Neighbours(nmax);
            faraccel(nmax);
            particles[nmax].gdto5 = AarsethTimeStep(particles[nmax].v, particles[nmax].w5, particles[nmax].w51, particles[nmax].w52);
        }

        particles[nmax].w = wn;
        particles[nmax].w1 = wn1;
        particles[nmax].w2 = wn2;

        AdvancePosition(&particles[nmax], dt);
        AdvanceVelocity(&particles[nmax], dt);
    }

    AdvancePosition(&particles[numi], dt);
    AdvanceVelocity(&particles[numi], dt);

    data.time += dt;
}

void NormalizeMomentum(Particle* particles, size_t count) {
    double totalMass = 0;
    Eigen::Vector3d totalMomentum(0, 0, 0);

    for (size_t i = 0; i < count; i++) {
        totalMomentum += particles[i].m * particles[i].v;
        totalMass += particles[i].m;
    }

    Eigen::Vector3d massCentreVelocity = totalMomentum / totalMass;
    for (size_t i = 0; i < count; i++) {
        particles[i].v = particles[i].v - massCentreVelocity;
    }
}

void PrecalculateAccelerationsAndItsDerivatives(Particle* particles, size_t count) {
    // Set acceleration, jerk and snap to zero.
    for (size_t i = 0; i < count; i++) {
        particles[i].w = Eigen::Vector3d(0, 0, 0);
        particles[i].w1 = Eigen::Vector3d(0, 0, 0);
        particles[i].w2 = Eigen::Vector3d(0, 0, 0);
    }

    // Calculate acceleration for all particles.
    for (size_t i = 0; i < count; i++) {
        for (size_t j = 0; j < count; j++) {
            if (i == j) {
                continue;
            }

            Eigen::Vector3d acceleration;
            CalculateAcceleration(particles[i], particles[j], &acceleration);
            particles[i].w += acceleration;
        }
    }

    // Calculate jerk and snap for all particles.
    for (size_t i = 0; i < count; i++) {
        for (size_t j = 0; j < count; j++) {
            if (i == j) {
                continue;
            }

            Eigen::Vector3d jerk;
            Eigen::Vector3d snap;
            CalculateJerkAndSnap(particles[i], particles[j], &jerk, &snap);

            particles[i].w1 += jerk;
            particles[i].w2 += snap;
        }
    }
}

void InitializeParticles() {
    std::vector<Particle>& particles = data.particles;

    NormalizeMomentum(&particles[0], particles.size());
    
    PrecalculateAccelerationsAndItsDerivatives(&particles[0], particles.size());
    
    for (size_t i = 0; i < particles.size(); i++) {
        FindNearest99Neighbours(i);
        faraccelf(i);
        particles[i].gdtof = AarsethTimeStep(particles[i].v, particles[i].wf, particles[i].wf1, particles[i].wf2);
        FindNearest3Neighbours(i);
        faraccel(i);
        particles[i].gdto5 = AarsethTimeStep(particles[i].v, particles[i].w5, particles[i].w51, particles[i].w52);
    }
     
    for (size_t i = 0; i < particles.size(); i++) {
        particles[i].gdt = AarsethTimeStep(particles[i].v, particles[i].w, particles[i].w1, particles[i].w2);
        particles[i].gdto = particles[i].gdt;
    }
}
