// Copyright (c) 2013 Nikita Kazeev and Alexander Konovalov. All rights
// reserved. Use of this source code is governed by a BSD-style license
// that can be found in the LICENSE file.

#ifndef CORE_MPI_ASSERT_H_
#define CORE_MPI_ASSERT_H_

#include <cassert>

extern void __mpi_assert_fail(__const char *__assertion, __const char *__file,
                              unsigned int __line, __const char *__function);

#define MPI_ASSERT(expr) \
  ((expr) \
   ? __ASSERT_VOID_CAST(0) \
   : __mpi_assert_fail(__STRING(expr), __FILE__, __LINE__, __PRETTY_FUNCTION__))

#endif  // CORE_MPI_ASSERT_H_
