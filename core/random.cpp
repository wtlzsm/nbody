// Copyright (c) 2013 Nikita Kazeev and Alexander Konovalov. All rights
// reserved. Use of this source code is governed by a BSD-style license
// that can be found in the LICENSE file.

#include "core/random.h"

#include <cstdlib>
#include <cmath>
#include <random>
#include <limits>
#include <iostream>
#include "core/mpi_assert.h"

namespace {
const float kFloatEpsilon = std::numeric_limits<float>::epsilon();

std::mt19937 randomGenerator;
std::uniform_real_distribution<float> uniformDistribution(0.0f, 1.0f - kFloatEpsilon);
}

float Random() {
  return uniformDistribution(randomGenerator);
}

float GaussianRandom(float mean, float dev) {
  float u = Random(), v = Random();
  float x = sqrt(-2 * log(u)) * cos(2 * M_PI * v);
  return mean + dev * x;
}

float PoissonRandom(float lambda) {
  // @see http://www.ece.virginia.edu/mv/edu/prob/stat/random-number-generation.pdf
  return (-1 / lambda) * ::log(1 - Random());
}

void RandomPointOnDisk(float maxRadius, float* angle, float* radius) {
  MPI_ASSERT(maxRadius > 0);
  // For explanation @see
  // http://www.ece.virginia.edu/mv/edu/prob/stat/random-number-generation.pdf
  // Since the total area of the tube front side is pi * r^2,
  // the PDF is p(x) = 2 * pi * r * dr / pi * R^2 == 2 * r * dr / R^2
  // and the CDF is F(x) = r^2 / R^2, thus F^-1(y) = sqrt(R^2 * y)
  *angle = 2 * M_PI * Random();
  *radius = sqrt(Random()) * maxRadius;

  if (*radius >= maxRadius)
    std::cout << *radius << " Max = " << maxRadius << '\n';
  MPI_ASSERT(*radius < maxRadius);
}
