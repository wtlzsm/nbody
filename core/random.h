// Copyright (c) 2013 Nikita Kazeev and Alexander Konovalov. All rights
// reserved. Use of this source code is governed by a BSD-style license
// that can be found in the LICENSE file.

#ifndef CORE_RANDOM_H_
#define CORE_RANDOM_H_

float Random();
float GaussianRandom(float mean = 0.0f, float dev = 1.0f);
float PoissonRandom(float lambda);
void RandomPointOnDisk(float maxRadius, float* angle, float* radius);

#endif  // CORE_RANDOM_H_
