#pragma once

// Solves Kepler's equation for elliptic orbit.
// @see https://en.wikipedia.org/wiki/Kepler%27s_Equation
double ksiel(double meanAnomaly, double eccentricity);

// Solves Hyperbolic Kepler's equation for elliptic orbit.
// @see https://en.wikipedia.org/wiki/Kepler%27s_Equation
double ksihyp(double meanAnomaly, double eccentricity);
