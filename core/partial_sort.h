#pragma once

#include <cassert>
#include <cstdlib>
#include <algorithm>

// Sorts some of the elements in the range [first, last) in ascending order.
// The first `middle - first` of these elements are placed in the range [first, middle).
// The order of equal elements is not guaranteed to be preserved.
// The order of the remaining elements in the range [middle, last) is unspecified.
template<class RandomIt>
void PartialSort(RandomIt begin, RandomIt middle, RandomIt end) {
    assert(begin <= middle && middle <= end);

    if (middle == begin || middle == end) {
        return;
    }

    RandomIt right = end - 1;
    RandomIt left = begin;

    while (left + 1 < right) {
        auto x = *middle;
        RandomIt i = left;
        RandomIt j = right;

        do {
            while (*i < x) {
                ++i;
            }

            while (x < *j) {
                assert(j > begin);
                --j;
            }

            if (i <= j) {
                std::swap(*i, *j);

                assert(j > begin);
                ++i;
                --j;
            }
        } while (i <= j);

        if (j < middle) {
            left = i;
        }

        if (middle < i) {
            right = j;
        }
    }
}

inline void PartialSort(double* rff, size_t middle, size_t size) {
    assert(rff != NULL);
    assert(middle < size);

    PartialSort(rff, rff + middle, rff + size);
}
