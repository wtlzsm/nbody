#pragma once

#include "win32_float_fix.h"

#include <vector>

#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cmath>
#include <cstdint>

#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Geometry>

struct Particle {
    double m;

    Eigen::Vector3d p;
    Eigen::Vector3d v;

    Eigen::Vector3d w;
    Eigen::Vector3d w1;
    Eigen::Vector3d w2;

    double gdt;
    double gdto;
    double currt;

    size_t num5[3];
    Eigen::Vector3d w5;
    Eigen::Vector3d w51;
    Eigen::Vector3d w52;
    double gdto5;

    size_t numf[99];
    Eigen::Vector3d wf;
    Eigen::Vector3d wf1;
    Eigen::Vector3d wf2;
    double gdtof;

    uint8_t red;
    uint8_t green;
    uint8_t blue;

    Particle()
        : m(0)
        , p(0, 0, 0)
        , v(0, 0, 0)
        , w(0, 0, 0)
        , w1(0, 0, 0)
        , w2(0, 0, 0)
        , gdt(0)
        , gdto(0)
        , currt(0)
        , w5(0, 0, 0)
        , w51(0, 0, 0)
        , w52(0, 0, 0)
        , gdto5(0)
        , wf(0, 0, 0)
        , wf1(0, 0, 0)
        , wf2(0, 0, 0)
        , gdtof(0)
        , red(0xFF)
        , green(0xFF)
        , blue(0xFF)
    { }

    Particle(double coord_x, double coord_y)
        : Particle()
    {
        p = Eigen::Vector3d(coord_x, coord_y, 0);
    }
};

struct ModelData {
    long double time;

    std::vector<Particle> particles;
    std::vector<double> tmp1;
    std::vector<double> tmp2;
};

extern ModelData data;

double CalculateTotalEnergy(Particle const* particles, size_t count);
Eigen::Vector3d CalculateTotalMomentum(Particle const* particles, size_t count);
Eigen::Vector3d CalculateTotalAngularMomentum(Particle const* particles, size_t count);

void TimeStep();
void InitializeParticles();
