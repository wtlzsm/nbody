// Copyright (c) 2013 Nikita Kazeev and Alexander Konovalov. All rights
// reserved. Use of this source code is governed by a BSD-style license
// that can be found in the LICENSE file.

#include "core/mpi_assert.h"

#include <mpi.h>

void __mpi_assert_fail(__const char *__assertion, __const char *__file,
                       unsigned int __line, __const char *__function) {
  fprintf(stderr, "Assertion failed:\n\t%s in\n%s, line\n%i in %s\n\n",
         __assertion, __file, __line, __function);
  MPI_Abort(MPI_COMM_WORLD, -1);
}
