#include "orbit_parameters.h"

#include "kepler_equations.h"

inline void RotateVector(Eigen::Vector3d const& n, double cos, double sin, Eigen::Vector3d const& v, Eigen::Vector3d* result) {
    result->coeffRef(0) = (cos + (1 - cos) * n[0] * n[0]) * v[0] +
                ((1 - cos) * n[0] * n[1] - sin * n[2]) * v[1] +
                ((1 - cos) * n[0] * n[2] + sin * n[1]) * v[2];

    result->coeffRef(1) = (cos + (1 - cos) * n[1] * n[1]) * v[1] +
                ((1 - cos) * n[1] * n[2] - sin * n[0]) * v[2] +
                ((1 - cos) * n[1] * n[0] + sin * n[2]) * v[0];

    result->coeffRef(2) = (cos + (1 - cos) * n[2] * n[2]) * v[2] +
                ((1 - cos) * n[2] * n[0] - sin * n[1]) * v[0] +
                ((1 - cos) * n[2] * n[1] + sin * n[0]) * v[1];
}

void cartel (
    double m1,
    double m2,
    Eigen::Vector3d const& p1,
    Eigen::Vector3d const& p2,
    Eigen::Vector3d const& v1,
    Eigen::Vector3d const& v2,
    double* a1,
    double* a2,
    double* e,
    Eigen::Vector3d* n,
    double* totn,
    Eigen::Vector3d* pc,
    Eigen::Vector3d* vc,
    Eigen::Vector3d* pulse)
{
    *pc = (m1 * p1 + m2 * p2) / (m1 + m2);
    *vc = (m1 * v1 + m2 * v2) / (m1 + m2);

    Eigen::Vector3d po = p2 - p1;
    Eigen::Vector3d vo = v2 - v1;
    Eigen::Vector3d pon = po / po.norm();

    double ao = po.norm() / (2 - vo.squaredNorm() * po.norm() / (m1 + m2));
    *a1 = ao * m2 / (m1 + m2);
    *a2 = ao * m1 / (m1 + m2);

    double reducedMass = m1 * m2 / (m1 + m2);
    Eigen::Vector3d Lo = reducedMass * po.cross(vo);
    *n = Lo / Lo.norm();

    double eo = sqrt(1 - Lo.squaredNorm() / reducedMass / ao / m1 / m2);
    *e = eo;

    double ksi = acos((ao - po.norm()) / ao / eo);
    if (po.dot(vo) < 0) {
        ksi = -ksi;
    }
    *totn = sqrt(ao * ao * ao / (m1 + m2)) * (ksi - eo * sin(ksi));

    double co = (cos(ksi) - eo) / (1 - eo * cos(ksi));
    double si = -1 * sqrt(1 - eo * eo) * sin(ksi) / (1 - eo * cos(ksi));
    RotateVector(*n, co, si, pon, pulse);
}

void carthyp(
    double m1,
    double m2,
    Eigen::Vector3d const& p1,
    Eigen::Vector3d const& p2,
    Eigen::Vector3d const& v1,
    Eigen::Vector3d const& v2,
    double* a1,
    double* a2,
    double* e,
    Eigen::Vector3d* n,
    double* totn,
    Eigen::Vector3d* pc,
    Eigen::Vector3d* vc,
    Eigen::Vector3d* pulse)
{
    typedef int numi, nmax;
    *pc = (m1 * p1 + m2 * p2) / (m1 + m2);
    *vc = (m1 * v1 + m2 * v2) / (m1 + m2);

    Eigen::Vector3d po = p2 - p1;
    Eigen::Vector3d vo = v2 - v1;
    Eigen::Vector3d pon = po / po.norm();

    double ao = -po.norm() / (2 - vo.squaredNorm() * po.norm() / (m1 + m2));
    *a1 = ao * m2 / (m1 + m2);
    *a2 = ao * m1 / (m1 + m2);

    double reducedMass = m1 * m2 / (m1 + m2);
    Eigen::Vector3d Lo = reducedMass * po.cross(vo);
    *n = Lo / Lo.norm();

    double eo = sqrt(1 + Lo.squaredNorm() / reducedMass / ao / m1 / m2);
    *e = eo;

    double chks = (po.norm() + ao) / ao / eo;
    double shks = sqrt(chks * chks - 1);

    double ksi;
    if (po.dot(vo) < 0) {
        shks = -shks;
        ksi = log(chks + shks);
    } else {
        ksi=log(chks + shks);
    }

    *totn = sqrt(ao * ao * ao / (m1 + m2)) * (eo * shks - ksi);
    assert(!isinf(*totn) && !isnan(*totn));

    double co = (eo - chks) / (eo * chks - 1);
    double si = -sqrt(eo * eo - 1) * shks / (eo * chks - 1);
    RotateVector(*n, co, si, pon,pulse);
}

void elcart(
    double a1,
    double a2,
    double e,
    Eigen::Vector3d const& n,
    double totn,
    Eigen::Vector3d const& pc,
    Eigen::Vector3d const& vc,
    Eigen::Vector3d const& pulse,
    double m1,
    double m2,
    Eigen::Vector3d* p1,
    Eigen::Vector3d* p2,
    Eigen::Vector3d* v1,
    Eigen::Vector3d* v2)
{
    double eo = e;
    double ao = a1 + a2;

    double semiMajorAxisLengthCubed = ao * ao * ao;

    // Mean anomaly
    double M = totn * sqrt((m1 + m2) / semiMajorAxisLengthCubed);

    double ksi = ksiel(M, e);

    double co = (cos(ksi) - eo) / (1 - eo * cos(ksi));
    double si = sqrt(1 - eo * eo) * sin(ksi) / (1 - eo * cos(ksi));
    Eigen::Vector3d pon;
    RotateVector(n, co, si, pulse, &pon);

    double ro = ao * (1 - e * cos(ksi));
    Eigen::Vector3d po = pon * ro;
    *p1 = -po * m2 / (m1 + m2) + pc;
    *p2 = po * m1 / (m1 + m2) + pc;

    double vx = -ao * sin(ksi);
    double vy = ao * sqrt(1 - eo * eo) * cos(ksi);
    double Vo = ao * sqrt(1 - eo * eo * cos(ksi) * cos(ksi));
    co = vx / Vo;
    si = vy / Vo;
    Eigen::Vector3d von;
    RotateVector(n, co, si, pulse, &von);
    Vo = sqrt((m1 + m2) * (-1 / ao + 2 / ro));
    Eigen::Vector3d vo = von * Vo;
    *v1 = -vo * m2 / (m1 + m2) + vc;
    *v2 = vo * m1 / (m1 + m2) + vc;
}

void hypcart(
    double a1,
    double a2,
    double e,
    Eigen::Vector3d const& n,
    double totn,
    Eigen::Vector3d const& pc,
    Eigen::Vector3d const& vc,
    Eigen::Vector3d const& p,
    double m1,
    double m2,
    Eigen::Vector3d* p1,
    Eigen::Vector3d* p2,
    Eigen::Vector3d* v1,
    Eigen::Vector3d* v2)
{
    double eo = e;
    double ao = a1 + a2;

    double semiMajorAxisLengthCubed = ao * ao * ao;

    // Mean anomaly
    double M = totn * sqrt((m1 + m2) / semiMajorAxisLengthCubed);

    double ksi = ksihyp(M, e);

    double eks = exp(ksi);
    double shks = (eks - 1 / eks) / 2;
    double chks = (eks + 1 / eks) / 2;
    double co = (eo - chks) / (eo * chks - 1);
    double si = sqrt(eo * eo - 1) * shks / (eo * chks - 1);
    Eigen::Vector3d pon;
    RotateVector(n, co, si, p, &pon);
    double ro = ao * (e * chks - 1);
    Eigen::Vector3d po = pon * ro;
    *p1 = -po * m2 / (m1 + m2) + pc;
    *p2 = po * m1 / (m1 + m2) + pc;
    double vx = -ao * shks;
    double vy = ao * sqrt(eo * eo - 1) * chks;
    double Vo = ao * sqrt(eo * eo * chks * chks - 1);
    co = vx / Vo;
    si = vy / Vo;
    Eigen::Vector3d von;
    RotateVector(n, co, si, p, &von);
    Vo = sqrt((m1 + m2)*(1 / ao + 2 / ro));
    Eigen::Vector3d vo = von * Vo;
    *v1 = -vo * m2 / (m1 + m2) + vc;
    *v2 = vo * m1 / (m1 + m2) + vc;
}
