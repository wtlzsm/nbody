#pragma once
#ifdef _MSC_VER

#include <float.h>
inline bool isnan(double x) {
    return _isnan(x) != 0;
}
inline bool isinf(double x) {
    return (!_finite(x));
}

#endif
